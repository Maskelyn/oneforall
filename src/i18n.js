// import i18n from "i18next";
// import { reactI18nextModule } from "react-i18next";

// // import translationEN from '../src/locales/en/translation.json';

// import translationEN from './translation.json';

// // the translations
// const resources = {
//   en: {
//     translation: translationEN
//   }
// };

// i18n
//   .use(reactI18nextModule) // passes i18n down to react-i18next
//   .init({
//     resources,
//     lng: "en",

//     keySeparator: false, // we do not use keys in form messages.welcome

//     interpolation: {
//       escapeValue: false // react already safes from xss
//     }
//   });

// export default i18n;


import i18n from 'i18next';
import moment from 'moment';
import { initReactI18next } from 'react-i18next';

import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
// don't want to use this?
// have a look at the Quick start guide 
// for passing in lng and translations on init 

i18n
  // load translation using http -> see /public/locales (i.e. https://github.com/i18next/react-i18next/tree/master/example/react/public/locales)
  // learn more: https://github.com/i18next/i18next-http-backend
  .use(Backend)
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  .use(LanguageDetector)
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    fallbackLng: 'en',
    debug: true,

    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    }
  }, (err, t) => {
    moment.locale(i18n.language);
  })
  ;


export default i18n;